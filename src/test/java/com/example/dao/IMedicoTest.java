package com.example.dao;

import static org.junit.Assert.*;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

import com.example.model.Medico;


@RunWith(SpringRunner.class)
@SpringBootTest
public class IMedicoTest {


	@Autowired
	private IMedicoDAO dao;
 

	@Test
	public void crearMedico() {
		Medico us = new Medico();
		us.setIdMedico(1);
		us.setNombres("kevin");
		us.setApellidos("meneses");
		us.setCMP("1544656");  

		dao.save(us);
		assertTrue(dao.count()>0);

	} 

	@Test
	public void buscarMedico() {

		Medico us = new Medico();
		us.setIdMedico(5);
		us.setNombres("kevin arthur");
		us.setApellidos("meneses rivera");
		us.setCMP("123456");  

		dao.save(us);

		assertTrue(dao.count()>0);

	}
	 
	@Test
	public void ListMedico() {

		Medico us = new Medico();
		us.setIdMedico(11);
		us.setNombres("kevin");
		us.setApellidos("meneses");
		us.setCMP("12313123");  
		dao.save(us);

		
		System.out.println("dao.count 1"+dao.count());
		Medico us2= new Medico();
		us2.setIdMedico(12);
		us2.setNombres("kevin");
		us2.setApellidos("meneses");
		us2.setCMP("1235646");
		dao.save(us2);
		System.out.println("dao.count 2"+dao.count());

		dao.delete(us2);

		System.out.println("dao.count 3"+dao.count());
		assertTrue(us.getApellidos()!=null);
		assertTrue(us.getCMP()!=null);
		assertTrue(us.getNombres()!=null);
		assertTrue(dao.count()>0);

	}
	
}
