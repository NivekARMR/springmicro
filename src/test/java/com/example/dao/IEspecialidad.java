package com.example.dao;

import static org.junit.Assert.*;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

import com.example.model.Especialidad;

@RunWith(SpringRunner.class)
@SpringBootTest
public class IEspecialidad {

	@Autowired
	private IEspecialidadDAO especialidaddao;
	
	@Test
	public void test() {

		Especialidad especialidad = new Especialidad();
		especialidad.setIdEspecialidad(1);
		especialidad.setNombre("Urologia");
		Especialidad nuevo=especialidaddao.save(especialidad);

		assertTrue(nuevo.getNombre()!=null);
		assertTrue(nuevo.getIdEspecialidad()!=0); 
	}

}
