package com.example.exception;

import static org.junit.Assert.*;

import java.sql.Timestamp;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import org.junit.Before;
import org.junit.Test;

import com.example.util.MyLocalDateConverter;
import com.example.util.MyLocalDateTimeConverter;
 
public class ExceptionTest {

	ExceptionResponse exception;
	ModeloNotFoundException modelo; 
	
	@Before
	public void setUp() throws Exception {
		
	exception = new ExceptionResponse(new Date(),"Error excetion","detail Error");
	exception.setDetalles("Error!!!");
	exception.setMensaje("Detail Error!!!");
	exception.setDetalles("Error!!!");
	exception.setTimestamp(new Date());

	modelo= new ModeloNotFoundException("ERROR");

	long millis=System.currentTimeMillis();
	java.sql.Date date2=new java.sql.Date(millis);

	MyLocalDateConverter mldc= new MyLocalDateConverter();
	 assertTrue(mldc.convertToDatabaseColumn(LocalDate.now())!=null);
	 assertTrue(mldc.convertToEntityAttribute(date2)!=null); 	

	 assertTrue(mldc.convertToDatabaseColumn(null)==null);
	 assertTrue(mldc.convertToEntityAttribute(null)==null); 	

	 MyLocalDateTimeConverter dtc= new MyLocalDateTimeConverter();
	 assertTrue(dtc.convertToDatabaseColumn(null)==null);
	 assertTrue(dtc.convertToEntityAttribute(null)==null);
		 assertTrue(dtc.convertToDatabaseColumn(LocalDateTime.now())!=null);
	 assertTrue(dtc.convertToEntityAttribute(new Timestamp(System.currentTimeMillis()))!=null);
 
	}

	@Test
	public void test() {
		assertTrue(exception.getDetalles()!=null);
		assertTrue(exception.getMensaje()!=null);
		assertTrue(exception.getDetalles()!=null);
		assertTrue(exception.getTimestamp()!=null);
	
	}

}
