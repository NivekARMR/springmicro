package com.example.controller;

import static org.junit.Assert.*; 
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;

import org.junit.Before;
import org.junit.Test;
import org.springframework.http.MediaType;
import org.springframework.test.web.servlet.MvcResult;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;
import com.example.AbstractTest;
import com.example.model.Especialidad;
import com.example.model.Medico;

import org.junit.Test;

public class MedicoControllerTest extends AbstractTest  {

	  @Override
	   @Before
	   public void setUp() {
	      super.setUp();
	   }
	  
	  @Test
	   public void getMedicos() throws Exception {
	      String uri = "/medicos";
	      MvcResult mvcResult = mvc.perform(MockMvcRequestBuilders.get(uri)
	         .accept(MediaType.APPLICATION_JSON_VALUE)).andReturn();
	      
	      int status = mvcResult.getResponse().getStatus();
	      assertEquals(200, status);
	      String content = mvcResult.getResponse().getContentAsString();
	      Medico[] medicolist = super.mapFromJson(content, Medico[].class);
	      assertTrue(medicolist.length>0);
	   }
	  
	  @Test
	   public void registrarMedico() throws Exception {
	      String uri = "/medicos";
	      Medico medico = new Medico();
	      medico.setApellidos("Meneses");
	      medico.setIdMedico(1);
	      medico.setNombres("Kevin");
	      medico.setCMP("123456"); 
	      String inputJson = super.mapToJson(medico);
	      MvcResult mvcResult = mvc.perform(MockMvcRequestBuilders.post(uri)
	         .contentType(MediaType.APPLICATION_JSON_VALUE)
	         .content(inputJson)).andReturn();
	      
	      int status = mvcResult.getResponse().getStatus();
	      System.out.println("registrar status"+status); 
	      assertEquals(201, status);
	      String content = mvcResult.getResponse().getContentAsString();
	   }
	  

		  @Test
		   public void actualizaMedico() throws Exception {
		      String uri = "/medicos";
		      Medico medico = new Medico();
		      medico.setApellidos("Meneses");
		      medico.setIdMedico(1);
		      medico.setNombres("Kevin");
		      medico.setCMP("123456"); 
		      String inputJson = super.mapToJson(medico);
		      MvcResult mvcResult = mvc.perform(MockMvcRequestBuilders.put(uri)
		         .contentType(MediaType.APPLICATION_JSON_VALUE)
		         .content(inputJson)).andReturn();
		      
		      int status = mvcResult.getResponse().getStatus();
		      System.out.println("status"+status); 
		      assertEquals(200, status);
		      String content = mvcResult.getResponse().getContentAsString();
		   }
		  
	   
	   }
