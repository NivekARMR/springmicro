package com.example.controller;

import static org.junit.Assert.*; 
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;

import org.junit.Before;
import org.junit.Test;
import org.springframework.http.MediaType;
import org.springframework.test.web.servlet.MvcResult;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;
import com.example.AbstractTest;
import com.example.model.Especialidad;
import com.example.model.Paciente;

import org.junit.Test;

public class PacienteControllerTest extends AbstractTest  {

	  @Override
	   @Before
	   public void setUp() {
	      super.setUp();
	   }
	  
	  @Test
	   public void getPacientes() throws Exception {
	      String uri = "/pacientes";
	      MvcResult mvcResult = mvc.perform(MockMvcRequestBuilders.get(uri)
	         .accept(MediaType.APPLICATION_JSON_VALUE)).andReturn();
	      
	      int status = mvcResult.getResponse().getStatus();
	      assertEquals(200, status);
	      String content = mvcResult.getResponse().getContentAsString();
	      Paciente[] especialidadlist = super.mapFromJson(content, Paciente[].class);
	      assertTrue(especialidadlist.length>0);
	   }
	  
	  @Test
	   public void registrarPaciente() throws Exception {
	      String uri = "/pacientes";
	      Paciente paciente = new Paciente();
	      paciente.setApellidos("Meneses");
	      paciente.setNombres("Kevin");
	      paciente.setDireccion("Jr Varela");
	      paciente.setTelefono("957264681");
	      paciente.setIdPaciente(1);
	      paciente.setDni("71819234");
	      
	      String inputJson = super.mapToJson(paciente);
	      MvcResult mvcResult = mvc.perform(MockMvcRequestBuilders.post(uri)
	         .contentType(MediaType.APPLICATION_JSON_VALUE)
	         .content(inputJson)).andReturn();
	      
	      int status = mvcResult.getResponse().getStatus();
	      System.out.println("registrar status"+status); 
	      assertEquals(201, status);
	      String content = mvcResult.getResponse().getContentAsString();
	   }
	  

		  @Test
		   public void actualizaPaciente() throws Exception {
		      String uri = "/pacientes";
		      Paciente paciente = new Paciente();
		      paciente.setApellidos("Meneses");
		      paciente.setNombres("Kevin");
		      paciente.setDireccion("Jr Varela");
		      paciente.setTelefono("957264681");
		      paciente.setIdPaciente(1);
		      paciente.setDni("71819234");
		      String inputJson = super.mapToJson(paciente);
		      MvcResult mvcResult = mvc.perform(MockMvcRequestBuilders.put(uri)
		         .contentType(MediaType.APPLICATION_JSON_VALUE)
		         .content(inputJson)).andReturn();
		      
		      int status = mvcResult.getResponse().getStatus();
		      System.out.println("status"+status); 
		      assertEquals(200, status);
		      String content = mvcResult.getResponse().getContentAsString();
		   }
		  
	   
	   }
