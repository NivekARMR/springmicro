package com.example.dao;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.example.model.Medico;

@Repository
public interface IMedicoDAO extends JpaRepository<Medico, Integer> {

}
