# README #

This repo contains a component simple crud microservice of a demo medic application.

### What is this repository for? ###

* Jar springboot component
* Beta version
* https://bitbucket.org/NivekARMR/mediapp-backend

### How do I get set up? ###

* Modify database field of application.properties
* Dependencies
* Database Postgresql.
* Run Testcases with Junit.
* Run docker container.
