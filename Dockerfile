FROM openjdk:11.0.6-jdk
LABEL manteinainer="se.kevinmenesesrivera@gmail.com"
WORKDIR /workspace
RUN ls -la /workspace
COPY /target/springmicrocrud-0.0.1-SNAPSHOT.jar springmicrocrud-0.0.1-SNAPSHOT.jar
RUN ls -la /workspace
EXPOSE 8912
ENTRYPOINT exec java -jar /workspace/springmicrocrud-0.0.1-SNAPSHOT.jar